﻿namespace GoogleImageDataSet.Core
{
    /// <summary>
    /// A static class with library constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The default number of images to fetch
        /// </summary>
        public const int DefaultImagesToFetch = 100;
        /// <summary>
        /// The default training ratio.
        /// </summary>
        public const double DefaultTrainingRatio = 0.85;
    }
}
