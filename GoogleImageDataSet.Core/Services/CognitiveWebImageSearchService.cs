﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;
using Newtonsoft.Json;
using unirest_net.http;

namespace GoogleImageDataSet.Core.Services
{
    public class CognitiveWebImageSearchService : IImageSearchService<BasicImageResult,BasicImageResultList>
    {
        //public const string BaseUri = "https://contextualwebsearch-websearch-v1.p.mashape.com/api/Search/ImageSearchAPI?autoCorrect=true&count=50&q=House";
        public const string BaseUri = "https://contextualwebsearch-websearch-v1.p.mashape.com/api/Search/ImageSearchAPI?autoCorrect=true";

        public const string MashapeKey = "r9Mii6x5pxmsh6hLYQ1eH60bUuxbp1YqTDhjsntu8a3DjZodFe";
        //public int MaxImages { get; set; } = 100;

        public BasicImageResultList Search(string searchTerm, int imagesToFetch)
        {
            //var uri = $"{BaseUri}&count={MaxImages}&q={HttpUtility.UrlEncode(searchTerm)}";
            var uri = $"{BaseUri}&count={imagesToFetch}&q={HttpUtility.UrlEncode(searchTerm)}";
            var response = Unirest.get(uri)
                .header("X-Mashape-Key", MashapeKey)
                .header("Accept", "application/json")
                .asString();
            var resultsJson = response.Body;
            var results = JsonConvert.DeserializeObject<SearchResult>(resultsJson);
            return new BasicImageResultList(searchTerm, results.Images.Select(image => new BasicImageResult(image.Url)));
        }

        public class SearchResult
        {
            [JsonProperty("_type")]
            public string Type { get; set; }
            [JsonProperty("value")]
            public IList<ImageResult> Images { get; set; }
        }

        public class ImageResult
        {
            [JsonProperty("url")]
            public string Url { get; set; }
            [JsonProperty("width")]
            public int Width { get; set; }
            [JsonProperty("height")]
            public int Height { get; set; }
        }
    }
}
