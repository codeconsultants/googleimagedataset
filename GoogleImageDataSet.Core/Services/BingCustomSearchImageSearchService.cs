﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using GoogleImageDataSet.Core.Extensions;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;
using Newtonsoft.Json;

namespace GoogleImageDataSet.Core.Services
{
    public class BingCustomSearchImageSearchService : IImageSearchService<BasicImageResult,BasicImageResultList>
    {
        // NB: See example: https://docs.microsoft.com/en-us/azure/cognitive-services/bing-image-search/quickstarts/csharp
        protected const string BingCustomSearchApiKey = "7fd5c8daa6a2473898ec807d01b7bd14"; // NB: CCCustomSearch Key #1, as of 2018-06-04

        // Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
        // search APIs.  In the future, regional endpoints may be available.  If you
        // encounter unexpected authorization errors, double-check this value against
        // the endpoint for your Bing search instance in your Azure dashboard.
        //const string UriBase = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";
        //const string UriBase = "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0";
        const string UriBase = "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/images/search";

        // Used to return image search results including relevant headers
        protected struct SearchResult
        {
            public string JsonResult;
            public Dictionary<string, string> RelevantHeaders;
        }

        public BasicImageResultList Search(string searchTerm, int imagesToFetch = 50)
        {
            throw new NotImplementedException("BingCustomSearch implementation is incomplete, and untested, and requires image batching and further testing/development");

            CheckKey();

            Console.WriteLine("Searching images for: " + searchTerm);

            SearchResult result = BingImageSearch(searchTerm);

            Console.WriteLine("\nRelevant HTTP Headers:\n");
            foreach (var header in result.RelevantHeaders)
                Console.WriteLine(header.Key + ": " + header.Value);

            Console.WriteLine("\nJSON Response:\n");
            Console.WriteLine(result.JsonResult.JsonPrettyPrint());

            var typedResult = JsonConvert.DeserializeObject<BingCustomSearchImageSearchResponse>(result.JsonResult);

            //var urls = typedResult.Value.Select(value => value.ContentUrl);
            var urls = typedResult.Value.Select(value => value.ThumbnailUrl);

            return new BasicImageResultList(searchTerm, urls.Select(url => new BasicImageResult(url)));
        }

        protected void CheckKey()
        {
            if(BingCustomSearchApiKey.Length != 32)
                throw new Exception($"Invalid Bing Custom Search Key: {BingCustomSearchApiKey}");
        }

        protected SearchResult BingImageSearch(string searchQuery, int imagesToFetch = 50)
        {
            // Construct the URI of the search request
            var uriQuery = UriBase + "?q=" + Uri.EscapeDataString(searchQuery);

            throw new NotImplementedException("Bing image searching is incomplete, as it needs image batch building, and didn;t seem to work against current APIs");

            // Perform the Web request and get the response
            var request = (HttpWebRequest)WebRequest.Create(uriQuery);
            request.Headers["Ocp-Apim-Subscription-Key"] = BingCustomSearchApiKey;

            Console.WriteLine($"Requesting URL: {uriQuery}");
            Console.WriteLine($"With header Ocp-Apim-Subscription-Key={BingCustomSearchApiKey}");

            var response = (HttpWebResponse)request.GetResponseAsync().Result;
            var json = new StreamReader(response.GetResponseStream()).ReadToEnd();

            // Create result object for return
            var searchResult = new SearchResult()
            {
                JsonResult = json,
                RelevantHeaders = new Dictionary<string, string>()
            };

            // Extract Bing HTTP headers
            foreach (string header in response.Headers)
            {
                if (header.StartsWith("BingAPIs-") || header.StartsWith("X-MSEdge-"))
                    searchResult.RelevantHeaders[header] = response.Headers[header];
            }

            return searchResult;
        }
    }
}
