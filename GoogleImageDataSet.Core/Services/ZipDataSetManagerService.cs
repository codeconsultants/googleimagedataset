﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;

namespace GoogleImageDataSet.Core.Services
{
    public class ZipDataSetManagerService : IDataSetManagerService<BasicImageResult>
    {
        public string ArchivePath { get; protected set; }

        public ZipDataSetManagerService(string archivePath)
        {
            if (!Directory.Exists(archivePath))
                throw new Exception($"DataSet Directory does not exist: {archivePath}");
            ArchivePath = archivePath;
        }

        public List<string> ListDataSets()
        {
            var zips = Directory.EnumerateFiles(ArchivePath, "*.zip");
            return zips.Select(Path.GetFileNameWithoutExtension).ToList();
        }

        protected const string ReadmeText = "This archive was created with Code-Consultants's GoogleImageDataSet tool";

        public bool CreateDataSet(string dataSetName)
        {
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
            using (var zipStream = File.Open(Path.Combine(ArchivePath, $"{dataSetName}.zip"), FileMode.CreateNew))
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
            using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Create))
            {
                var entry = zip.CreateEntry("README.md");
                using (var fileStream = entry.Open())
                using (var writer = new StreamWriter(fileStream))
                {
                    writer.WriteLine(ReadmeText);
                    writer.Flush();
                }
            }
            return true;
        }

        public bool DeleteDataSet(string dataSetName)
        {
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
            File.Delete(Path.Combine(ArchivePath, $"{dataSetName}.zip"));
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
            return true;
        }

        protected string GetEntryFolderPath(bool training, string imageSetName)
            => $"{(training ? "train/" : "valid/")}{imageSetName}/";

        protected string GetEntryPath(string path, string imageSetName, int index)
            => $"{path}{imageSetName}-{index:d5}.jpg";

        public bool AddImage(string dataSetName, string imageSetName, bool training, BasicImageResult imageResult)
        {
            using (var webClient = new WebClient())
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
            using (var zipStream = File.Open(Path.Combine(ArchivePath, $"{dataSetName}.zip"), FileMode.Open))
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
            using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Update))
            {
                var path = GetEntryFolderPath(training, imageSetName);
                var index = 0;
                while (zip.Entries.Any(entry => entry.FullName == GetEntryPath(path, imageSetName, index)))
                    index++;
                var entryName = $"{path}{imageSetName}-{index:d5}.jpg";

                try
                {
                    var imageBytes = webClient.DownloadData(imageResult.Url);

                    // Create image from bytearray, to check the image is valid
                    var image = Image.Load(new MemoryStream(imageBytes));

                    var newEntry = zip.CreateEntry(entryName, CompressionLevel.Optimal);
                    var fileStream = newEntry.Open();
                    //fileStream.Write(imageBytes, 0, imageBytes.Length);
                    image.Save(fileStream, new JpegEncoder());

                    Console.WriteLine($"{imageResult.Url} written to {entryName}, {imageBytes.LongLength} bytes");
                }
                catch (WebException webEx)
                {
                    Console.WriteLine($"Error fetching {imageResult.Url}\r\n{webEx.Message}");
                }
                catch (NotSupportedException nsEx)
                {
                    Console.WriteLine($"Image was not in a supported format: {imageResult.Url}\r\n{nsEx.Message}");
                }
            }
            return true;
        }

        public IList<string> ListCategories(string dataSetName)
        {
            var categories = new List<string>();
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
            using (var zipStream = File.Open(Path.Combine(ArchivePath, $"{dataSetName}.zip"), FileMode.Open))
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
            using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Read))
                foreach (var entry in zip.Entries)
                {
                    var parts = entry.FullName.Split('/');
                    // If we are in "train/" or "valid/", check for the next folder name and stash as a category if missing.
                    if (parts.Length <= 2 || parts[0] != "train" && parts[0] != "valid" || categories.Contains(parts[1])) continue;
                    categories.Add(parts[1]);
                }
            return categories;
        }
    }
}
