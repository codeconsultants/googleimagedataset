﻿using System.Collections.Generic;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;

namespace GoogleImageDataSet.Core.Services
{
    /// <summary>
    /// A dummy (always succeed or fails)
    /// </summary>
    public class DummyDataSetManagerService : IDataSetManagerService<BasicImageResult>, IHasExists
    {
        /// <summary>
        /// Gets or sets a value indicating whether [should succeed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [should succeed]; otherwise, <c>false</c>.
        /// </value>
        public bool ShouldSucceed { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether method calls will throw exceptions.
        /// </summary>
        /// <value>
        ///   <c>true</c> to throw exceptions, otherwise, <c>false</c>.
        /// </value>
        public bool ThrowExceptions { get; protected set; }

        /// <summary>
        /// Gets or sets the dummy data set names.
        /// </summary>
        /// <value>
        /// The dummy data set names.
        /// </value>
        public List<string> DummyDataSetNames { get; protected set; }
        /// <summary>
        /// Gets or sets the dummy category names.
        /// </summary>
        /// <value>
        /// The dummy category names.
        /// </value>
        public List<string> DummyCategoryNames { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DummyDataSetManagerService"/> class.
        /// </summary>
        public DummyDataSetManagerService()
            : this(true)
        { }

        public static readonly string[] DefaultDataSetNames = { "DataSet1", "DataSet2", "Examples1", "Others2" };
        public static readonly string[] DefaultCategoryNames = { "Donald Trump", "Whisky", "Space", "Animal" };

        /// <summary>
        /// Initializes a new instance of the <see cref="DummyDataSetManagerService" /> class.
        /// </summary>
        /// <param name="shouldSucceed">if set to <c>true</c> the dummy calls should succeed.</param>
        /// <param name="throwExceptions">if set to <c>true</c> [throw exceptions].</param>
        /// <param name="dummyDataSets">specified the dataset names to return (if set to succeed and not throw errors)</param>
        protected DummyDataSetManagerService(bool shouldSucceed, bool throwExceptions = false, params string[] dummyDataSets)
        {
            ShouldSucceed = shouldSucceed;
            ThrowExceptions = throwExceptions;
            DummyDataSetNames = new List<string>(dummyDataSets ?? DefaultDataSetNames);
            DummyCategoryNames = new List<string>(DefaultCategoryNames);
        }

        protected T DummyMethodCall<T>(T successValue, T failureValue)
        {
            if(ThrowExceptions)
                throw new System.NotImplementedException($"Dummy service {GetType().FullName} is set to throw exceptions");
            return ShouldSucceed ? successValue : failureValue;
        }
        protected bool DummyMethodCall() => DummyMethodCall(true, false);

        public List<string> ListDataSets() => DummyMethodCall(DummyDataSetNames, null);
        public bool CreateDataSet(string dataSetName) => DummyMethodCall();
        public bool DeleteDataSet(string dataSetName) => DummyMethodCall();
        public bool AddImage(string dataSetName, string imageSetName, bool training, BasicImageResult imageResult) => DummyMethodCall();
        public IList<string> ListCategories(string dataSetName) => DummyMethodCall(DummyCategoryNames, new List<string>());
        // IHasExists:-
        public bool Exists(string dataSetName) => DummyMethodCall();
    }
}
