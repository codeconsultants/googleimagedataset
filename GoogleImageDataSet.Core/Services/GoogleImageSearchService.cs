﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Services;
using GoogleImageDataSet.Core.Extensions;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;
using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;
using Knyaz.Optimus;
using Knyaz.Optimus.Dom.Css;
using Knyaz.Optimus.Dom.Events;
using Knyaz.Optimus.Dom.Interfaces;
using Knyaz.Optimus.Environment;
using Knyaz.Optimus.TestingTools;
using Newtonsoft.Json;
using Console = System.Console;

/* Custom Search version using official package:-
using Google.Apis.Discovery.v1;
using Google.Apis.Discovery.v1.Data;
using Google.Apis.Services;
//*/
//* google-api-for-dotnet version:-

namespace GoogleImageDataSet.Core.Services
{
    public partial class GoogleImageSearchService : IImageSearchService<BasicImageResult, BasicImageResultList>
    {
        protected const string GoogleApiKey = "AIzaSyAvHdaFp4aXzRdxaHvo6aZJn2Zap655cFs"; // NB: API key 1, in project fastai, as of 2018-06-04

        /* Custom Search version using official package:-
        using Google.Apis.Discovery.v1;
        using Google.Apis.Discovery.v1.Data;
        using Google.Apis.Services;
        //*/

        /* Custom Search version using official package
        const string UriBase = "https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/images/search";

        // Used to return image search results including relevant headers
        protected struct SearchResult
        {
            public IList<string> Items;
            //public string JsonResult;
            //public Dictionary<string, string> RelevantHeaders;
            public SearchResult(IEnumerable<string> items)
            {
                Items = items?.ToList();
            }
        }

        public BasicImageResultList Search(string searchTerm)
        {
            CheckKey();

            Console.WriteLine("Searching images for: " + searchTerm);

            SearchResult result = GoogleImageSearch(searchTerm);

            Console.WriteLine("\nRelevant HTTP Headers:\n");
            foreach (var header in result.RelevantHeaders)
                Console.WriteLine(header.Key + ": " + header.Value);

            Console.WriteLine("\nJSON Response:\n");
            Console.WriteLine(result.JsonResult.JsonPrettyPrint());

            //var typedResult = JsonConvert.DeserializeObject<GoogleCustomSearchResponse>(result.JsonResult);

            //var urls = typedResult.Value.Select(value => value.ContentUrl);
            //var urls = typedResult.Value.Select(value => value.ThumbnailUrl);

            //return new BasicImageResultList(searchTerm, urls.Select(url => new BasicImageResult(url)));
            return null;
        }

        protected void CheckKey()
        {
            if(string.IsNullOrWhiteSpace(GoogleApiKey))
                throw new Exception($"Invalid Google API Key: {GoogleApiKey}");
        }

        protected SearchResult GoogleImageSearch(string searchQuery)
        {
            var service = new DiscoveryService(new BaseClientService.Initializer
            {
                ApplicationName = "GoogleImageDataSet",
                ApiKey = GoogleApiKey
            });

            var result = service.Apis.List().Execute();

            if(result.Items != null)
                return new SearchResult(result.Items.Select(itemData => itemData.Name));
        }
        //*/

        //public int MaxImages { get; set; } = 50;
        public int Offset { get; set; } = 0;

        public class NavigatorWithCustomUserAgent : INavigator
        {
            private readonly INavigator _navigator;
            public NavigatorWithCustomUserAgent(INavigator navigator, string userAgent)
            {
                UserAgent = userAgent;
                _navigator = navigator;
            }

            public bool JavaEnabled() => _navigator.JavaEnabled();

            public string AppCodeName => _navigator.AppCodeName;
            public string AppName => _navigator.AppName;
            public string AppVersion => _navigator.AppVersion;
            public bool CookieEnabled => _navigator.CookieEnabled;
            public string Geolocation => _navigator.Geolocation;
            public bool OnLine => _navigator.OnLine;
            public string Platform => _navigator.Platform;
            public string Product => _navigator.Product;
            public string UserAgent { get; }
            public string Language => Language;
        }
        //public class WindowWithCustomUserAgent : Window
        //{
        //    protected readonly Window Window;
        //    private readonly INavigator _navigator;
        //    public WindowWithCustomUserAgent(Window window, string userAgent)
        //        : base(() => null, null, null)
        //    {
        //        Window = window;
        //        _navigator = new NavigatorWithCustomUserAgent(window.Navigator, userAgent);
        //    }

        //    public new void AddEventListener(string type, Action<Event> listener, bool useCapture) => Window
        //        .AddEventListener(type, listener, useCapture);

        //    public void RemoveEventListener(string type, Action<Event> listener, bool useCapture) => Window
        //        .RemoveEventListener(type, listener, useCapture);

        //    public bool DispatchEvent(Event evt) => Window.DispatchEvent(evt);

        //    public int SetTimeout(Action<object> handler, double? delay, object ctx) => Window
        //        .SetTimeout(handler, delay, ctx);

        //    public void ClearTimeout(int handle) => Window.ClearTimeout(handle);

        //    public int SetInterval(Action handler, double? delay) => Window.SetInterval(handler, delay);

        //    public void ClearInterval(int handle) => Window.ClearInterval(handle);

        //    public ICssStyleDeclaration GetComputedStyle(IElement element) => Window.GetComputedStyle(element);

        //    public ICssStyleDeclaration GetComputedStyle(IElement element, string pseudoElt) => Window
        //        .GetComputedStyle(element, pseudoElt);

        //    public MediaQueryList MatchMedia(string query) => Window.MatchMedia(query);

        //    public int InnerWidth {
        //        get => Window.InnerWidth;
        //        set => Window.InnerWidth = value;
        //    }

        //    public int InnerHeight
        //    {
        //        get => Window.InnerHeight;
        //        set => Window.InnerHeight = value;
        //    }

        //    public IScreen Screen => Window.Screen;
        //    public Location Location => Window.Location;
        //    //public INavigator Navigator => Window.Navigator
        //    public INavigator Navigator => _navigator;
        //    public IHistory History => Window.History;
        //}
        public class OptimusEngineWithCustomUserAgent : Engine
        {
            //public new Window Window { get; }
            public OptimusEngineWithCustomUserAgent(string userAgent)
            {
                //Window = new WindowWithCustomUserAgent(base.Window, userAgent);
                
                typeof(Window).GetProperty("Navigator").SetValue(Window, new NavigatorWithCustomUserAgent(Window.Navigator, userAgent), null);
                
                //Window.GetType().GetProperty("Navigator")
                //    .SetValue(Window.Navigator, (INavigator)new NavigatorWithCustomUserAgent(Window.Navigator, userAgent), null);
            }
        }

        public BasicImageResultList Search(string searchTerm, int imagesToFetch = 50)
        {
            //throw new NotImplementedException();
            //return null;

            //var url = https://www.google.com/search?q=house&start=0&ndsp=1000&tbm=isch&gws_rd=ssl"
            var baseUrl = "https://www.google.com/search?tbm=isch&gws_rd=ssl";
            //var url = $"{baseUrl}&q={searchTerm}&start={Offset}&ndsp={MaxImages}";
            //var url = $"{baseUrl}&q={searchTerm}&start={Offset}";
            var url = $"{baseUrl}&q={searchTerm}";
            // Add additional options: see https://stenevang.wordpress.com/2013/02/22/google-advanced-power-search-url-request-parameters/

            //url += "&tbs=itp:photo"; // Photos only
            //url += "&tbs=itp:face"; // Faces only
            //url += "&tbs=itc:color"; // Colour only
            //url += "&tbs=itc:gray"; // B&W only

            /*
            Large images: tbs=isz:l
            Medium images: tbs=isz:m
            Icon sized images: tba=isz:i
            Image size larger than 400×300: tbs=isz:lt,islt:qsvga
            Image size larger than 640×480: tbs=isz:lt,islt:vga
            Image size larger than 800×600: tbs=isz:lt,islt:svga
            Image size larger than 1024×768: tbs=isz:lt,islt:xga
            Image size larger than 1600×1200: tbs=isz:lt,islt:2mp
            Image size larger than 2272×1704: tbs=isz:lt,islt:4mp
            Image sized exactly 1000×1000: tbs=isz:ex,iszw:1000,iszh:1000
            Images in full color: tbs=ic:color
            Images in black and white: tbs=ic:gray
            Images that are red: tbs=ic:specific,isc:red [orange, yellow, green, teal, blue, purple, pink, white, gray, black, brown]
            Image type Face: tbs=itp:face
            Image type Photo: tbs=itp:photo
            Image type Clipart: tbs=itp:clipart
            Image type Line drawing: tbs=itp:lineart
            Image type Animated (gif): tbs=itp:animated (thanks Dan)
            Group images by subject: tbs=isg:to
            Show image sizes in search results: tbs=imgo:1
            Example URL: Search in images for “michael jackson” as a phrase, and limit results to 4 megapixel images or larger, color images, face images, and group the results by topic:

            http://www.google.com/search?q=%22michael+jackson%22&tbm=isch&tbs=ic:color,isz:lt,islt:4mp,itp:face,isg:to
            */
            url += "&tbs=ic:color,isz:lt,islt:qsvga,itp:photo"; // Colour, photo, larger than 400x300
                                                                //url += "&ijn=1";    // Enables different format of results which includes full image links

            //url = $"https://www.google.com/search?ei=Fm46W732H8TOwAK1gZK4Bw&safe=off&tbs=ic:color,isz:lt,islt:qsvga,itp:photo&yv=3&tbm=isch&vet=10ahUKEwi92byShYHcAhVEJ1AKHbWABHc4AhC5PQjqASgB.Fm46W732H8TOwAK1gZK4Bw.i&ved=0ahUKEwi92byShYHcAhVEJ1AKHbWABHc4AhC5PQjqASgB&ijn=1&asearch=ichunk&async=_id:rg_s,_pms:s,_fmt:pc&start=2&q=house"
            url =
                $"https://www.google.com/search?ei=Fm46W732H8TOwAK1gZK4Bw&safe=off&tbs=ic:color,isz:lt,islt:qsvga,itp:photo&yv=3&tbm=isch&vet=10ahUKEwi92byShYHcAhVEJ1AKHbWABHc4AhC5PQjqASgB.Fm46W732H8TOwAK1gZK4Bw.i&ved=0ahUKEwi92byShYHcAhVEJ1AKHbWABHc4AhC5PQjqASgB&ijn=1&asearch=ichunk&async=_id:rg_s,_pms:s,_fmt:pc&q={searchTerm}";

            Console.WriteLine($"GoogleImageSearchService - Grabbing images from: {url}");

            var imageUrls = new List<string>(imagesToFetch);
            var web = new HtmlWeb();
            web.UserAgent =
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

            /*
            //var engine = new Engine();
            var engine = new OptimusEngineWithCustomUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");

            //engine.Window.Navigator.UserAgent =
            //    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

            //typeof(Knyaz.Optimus.Dom.Interfaces.INavigator).GetProperty("UserAgent").SetValue(engine.Window.Navigator, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36", null);
            //((Knyaz.Optimus.Environment.Navigator)engine.Window.Navigator).UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";

            //engine.DocumentChanged += () => Console.WriteLine("Document Changed");
            engine.DocumentChanged += () =>
            {
                Console.WriteLine("Document Changed");
                Console.WriteLine($"Document={engine.Document.OuterHTML}");
            };
            engine.OnUriChanged += () => Console.WriteLine("Uri Changed");
            engine.OnWindowOpen += (p1, p2, p3) => Console.WriteLine($"Window Opened - {p1}, {p2}, {p3}");
            engine.PreHandleResponse += (sender, e) => Console.WriteLine($"Pre Handle Response - {sender}, {JsonConvert.SerializeObject(e)}");
            //*/

            // NB: Google returns only 20 images in each batch, so fetch and amend offset.
            var offset = Offset;
            const int batchSize = 100;
            int batchIndex = 0;
            while (imageUrls.Count < imagesToFetch)
            {
                //var batchUrl = url + $"&start={offset}";
                var batchUrl = url + $"&start={batchIndex++}";

                /* Knyaz.Optimus version (headless browser)
                ////engine.OpenUrl(batchUrl).RunSynchronously(TaskScheduler.Current);
                ////engine.OpenUrl(batchUrl).RunSynchronously();
                //Console.WriteLine("Opening URL");
                //var task = engine.OpenUrl(batchUrl);
                ////task.Wait();  // Exception
                ////task.ConfigureAwait(false); // Locks
                ////task.ConfigureAwait(true);
                //Console.WriteLine($"compeleted = {task.IsCompleted}");
                //while (!task.IsCompleted && !task.IsCanceled && !task.IsFaulted)
                //{
                //    Console.WriteLine("Waiting");
                //    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                //}
                //Console.WriteLine($"Complete: {task.IsCompleted}, Canceled: {task.IsCanceled}, Faulted: {task.IsFaulted}");
                //if (task.IsFaulted)
                //{
                //    Console.WriteLine(task.Exception.GetBaseException());
                //}

                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead(batchUrl))
                        engine.Load(stream);
                }

                engine.Window.Screen.Height = 1200;
                engine.Window.Screen.Width = 1600;

                if (engine.Document == null)
                {
                    Console.WriteLine("Document is null, so waiting for document load");
                    engine.WaitDocumentLoad();
                }

                // Sleep, and let the engine's JS kick in (hopefully)
                //Console.WriteLine("Sleeping to allow JS to kick in");
                //System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(100));

                //throw new Exception(engine.Document.OuterHTML);
                var metaElements = engine.WaitSelector("div.rg_meta", 3000);
                //var metaElements = engine.WaitSelector("div.rg_meta");
                throw new Exception("Count=" + metaElements.Count());
                var jsonData = metaElements.Select(meta => meta.TextContent);

                var metaData = jsonData.Select(JsonConvert.DeserializeObject<Models.GoogleImageMetadata>);

                var urls = metaData.Select(meta => meta.ou).ToList();
                //*/

                //* HtmlAgilityPack version:-
                //var doc = web.Load(batchUrl);
                var html = null as string;
                using (var client = new WebClient())
                {
                    try
                    {
                        html = client.DownloadString(batchUrl);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ERROR: Failed to fetch batch from URL: " + batchUrl + "\r\n" + ex.Message);
                        return null;
                    }
                    // Fix returned HTML
                    html = $"<html><body>{html.Substring(html.IndexOf("</style>") + 8)}</body></html>";
                }
                var doc = new HtmlAgilityPack.HtmlDocument();

                //File.WriteAllText("lastfetch.htm", html);
                //Console.WriteLine(html); return null;

                doc.LoadHtml(html);
                //throw new Exception(doc.DocumentNode.OuterHtml);
                //throw new Exception(doc.DocumentNode.SelectNodes(@"//div").Where(div => div.HasClass("rg_meta")).Count().ToString());

                //var selector = @"//img";
                //var nodes = doc.DocumentNode.SelectNodes(selector);
                var selector = @"//div";
                IEnumerable<HtmlNode> nodes = null;
                try
                {
                    //Console.WriteLine($"div nodes: {doc.DocumentNode.SelectNodes(selector).Count}"); return null;
                    //Console.WriteLine($"div nodes: {doc.DocumentNode.SelectNodes(selector).Where(div => div.HasClass("rg_meta")).Count()}"); return null;

                    //nodes = doc.DocumentNode.SelectNodes(selector).Where(div => div.HasClass("rg_meta"));

                    // NB: div with class rg_meta works on direct page, but not for JS fetch equivalent
                    //nodes = doc.DocumentNode.SelectNodes("//a//img")
                    //    .Where(img => img.GetAttributeValue("src", null)
                    //        .StartsWith("http://images.google.com/imgres?imgurl="));

                    //nodes = doc.QuerySelectorAll("a>img[src]");
                    nodes = doc.QuerySelectorAll("div.rg_bx>a[href]");
                    Console.WriteLine($"nodes.Count = {nodes.Count()}");
                }
                catch
                {
                    Console.WriteLine("ERROR: Failed to fetch " + batchUrl);
                }

                if (!(nodes?.Any() ?? false))
                {
                    Console.WriteLine($"No images returned for search \"{searchTerm}\" offset \"{offset}\"");
                    return null;
                }
                //File.WriteAllText(@".\lastgoogle.htm", doc.DocumentNode.OuterHtml);

                /* Version for HTML fetch, not JS equivalent version
                //var urls = nodes?.Select(a => a.GetAttributeValue("src", null)).Where(src => !string.IsNullOrEmpty(src)) ?? Enumerable.Empty<string>();
                var jsonData = nodes.Select(div => div.InnerText);
                Console.WriteLine(string.Join("\r\n", jsonData)); return null;
                var data = jsonData.Select(JsonConvert.DeserializeObject<GoogleImageMetadata>);
                var urls = data.Where(metadata => !string.IsNullOrEmpty(metadata.ou)).Select(metadata => metadata.ou).ToList();
                //*/

                //var srcs = nodes.Select(node => node.GetAttributeValue("src", null));
                //Console.WriteLine(string.Join("\r\n", srcs));

                var hrefs = nodes.Select(node => node.GetAttributeValue("href", null));
                //Console.WriteLine(string.Join("\r\n", hrefs));

                //var urls = srcs.Select(src => src.Substring(39, src.IndexOf("?") - 39)).ToList();
                //Console.WriteLine(string.Join("\r\n", urls));
                //var urls = hrefs.Select(href => href.Substring(39, href.IndexOf("?") - 39)).ToList();
                var urls = hrefs.Select(href => href.Substring(0, href.IndexOf("&amp;imgrefurl")).Substring(39));
                //Console.WriteLine(string.Join("\r\n", urls));

                // Common elements (adding from urls)
                var maxRemaining = imagesToFetch - imageUrls.Count;
                imageUrls.AddRange(urls.Take(Math.Min(maxRemaining, urls.Count())));

                if (urls.Count() < batchSize)
                    break;
                offset += Math.Min(maxRemaining, urls.Count());
                Console.WriteLine($"next offset: {offset}, imageUrls.Count: {imageUrls.Count}, imagesToFetch: {imagesToFetch}, (imageUrls.Count < imagesToFetch): {imageUrls.Count < imagesToFetch}");
            }

            return new BasicImageResultList(searchTerm, imageUrls.Select(imageUrl => new BasicImageResult(imageUrl)));
        }
        //*/
    }
}
