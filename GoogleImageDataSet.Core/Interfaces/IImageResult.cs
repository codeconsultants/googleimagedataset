﻿namespace GoogleImageDataSet.Core.Interfaces
{
    /// <summary>
    /// An interface for image [search] results
    /// </summary>
    public interface IImageResult
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        string Url { get; set; }
    }
}
