﻿using TIndex = System.Int32;

namespace GoogleImageDataSet.Core.Interfaces
{
    /// <summary>
    /// The interface for entries with an index value
    /// </summary>
    public interface IIndexed
    {
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        TIndex Index { get; set; }
    }

    /// <summary>
    /// The interace for typed value entries with an index value.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IIndexed<TValue> : IIndexed
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        TValue Value { get; set; }
    }
}
