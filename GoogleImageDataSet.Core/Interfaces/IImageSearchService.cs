﻿namespace GoogleImageDataSet.Core.Interfaces
{
    public interface IImageSearchService<TImageResult, out TImageResultList>
        where TImageResult : IImageResult
        where TImageResultList : IImageResultList<TImageResult>
    {
        TImageResultList Search(string searchTerm, int imagesToFetch = 50);
    }
}
