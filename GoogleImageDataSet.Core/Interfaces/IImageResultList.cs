﻿using System.Collections.Generic;

namespace GoogleImageDataSet.Core.Interfaces
{
    public interface IImageResultList<TImageResult> : IList<TImageResult>
        where TImageResult : IImageResult
    {
        /// <summary>
        /// Gets or sets the search term.
        /// </summary>
        /// <value>
        /// The search term.
        /// </value>
        string SearchTerm { get; set; }
    }
}
