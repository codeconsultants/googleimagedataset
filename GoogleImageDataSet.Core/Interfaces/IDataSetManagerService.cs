﻿using System.Collections.Generic;

namespace GoogleImageDataSet.Core.Interfaces
{
    /// <summary>
    /// An interface for data-set manager services.
    /// </summary>
    /// <typeparam name="TImageResult">The type of the image result.</typeparam>
    public interface IDataSetManagerService<in TImageResult>
        where TImageResult : IImageResult
    {
        /// <summary>
        /// Lists the data sets.
        /// </summary>
        /// <returns></returns>
        List<string> ListDataSets();
        /// <summary>
        /// Creates the data set.
        /// </summary>
        /// <param name="dataSetName">Name of the data set.</param>
        /// <returns></returns>
        bool CreateDataSet(string dataSetName);
        /// <summary>
        /// Deletes the data set.
        /// </summary>
        /// <param name="dataSetName">Name of the data set.</param>
        /// <returns></returns>
        bool DeleteDataSet(string dataSetName);
        /// <summary>
        /// Adds the image.
        /// </summary>
        /// <param name="dataSetName">Name of the data set.</param>
        /// <param name="imageSetName">Name of the image set.</param>
        /// <param name="training">if set to <c>true</c> [training].</param>
        /// <param name="imageResult">The image result.</param>
        /// <returns></returns>
        bool AddImage(string dataSetName, string imageSetName, bool training, TImageResult imageResult);

        /// <summary>
        /// Lists the categories within a data set.
        /// </summary>
        /// <param name="dataSetName">Name of the data set.</param>
        /// <returns>The list of current categories.</returns>
        IList<string> ListCategories(string dataSetName);
    }
}
