﻿namespace GoogleImageDataSet.Core.Interfaces
{
    /// <summary>
    /// An implementation of IHasExists for string matching (such as name or key matching)
    /// </summary>
    /// <seealso cref="string" />
    public interface IHasExists : IHasExists<string>
    {
    }

    /// <summary>
    /// An interface for objects with an Exists method for the specified type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="string" />
    public interface IHasExists<in T>
    {
        /// <summary>
        /// Checks if a matched entity/entry exists.
        /// </summary>
        /// <param name="entityToMatch">The entity/entry to match.</param>
        /// <returns>True if the entry/entity exists.</returns>
        bool Exists(T entityToMatch);
    }
}
