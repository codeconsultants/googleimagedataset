﻿using System.Collections.Generic;
using System.Linq;
using GoogleImageDataSet.Core.Interfaces;
using GoogleImageDataSet.Core.Models;

namespace GoogleImageDataSet.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IIndexed<TValue>> IndexedValues<TValue>(this IEnumerable<TValue> values)
        {
            var enumerable = values as TValue[] ?? values.ToArray();
            for(var i = 0; i < enumerable.Length; i++)
                yield return new Indexed<TValue>(i, enumerable[i]);
        }
    }
}
