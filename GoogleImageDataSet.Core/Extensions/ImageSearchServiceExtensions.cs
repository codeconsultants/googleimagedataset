﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using GoogleImageDataSet.Core.Interfaces;

namespace GoogleImageDataSet.Core.Extensions
{
    public static class ImageSearchServiceExtensions
    {
        public static bool AddToDataSet<TImageResult, TImageResultList>(
            this IImageSearchService<TImageResult, TImageResultList> imageSearchService,
            IDataSetManagerService<TImageResult> dataSetManagerService, string dataSetName, string searchTerm, double trainingRatio = 0.7, bool overwrite = false, bool extendExisting = true, int imagesToFetch = 50)
            where TImageResult : IImageResult
            where TImageResultList : IImageResultList<TImageResult>
        {
            var results = imageSearchService.Search(searchTerm, imagesToFetch);
            return dataSetManagerService.SaveDataSet(dataSetName, trainingRatio, overwrite, extendExisting, results);
        }

        public static bool SaveToFolder<TImageResult, TImageResultList>(
            this IImageSearchService<TImageResult, TImageResultList> imageSearchService,
            string outputPath, int imagesToFetch, bool createSubFolders, bool deleteExisting, string searchTerm
        )
            where TImageResult : IImageResult
            where TImageResultList : IImageResultList<TImageResult>
        {
            var finalPath = createSubFolders ? Path.Combine(outputPath, searchTerm) : outputPath;
            if (!Directory.Exists(finalPath)) Directory.CreateDirectory(finalPath);

            var existingFiles = Directory.GetFiles(finalPath, $"{searchTerm}-*.jpg");
            int index = 0;
            if (existingFiles.Any())
            {
                // To avoid creating duplicate files, we either need to be instructed to add to existing (not currently implemented), or delete the existing matching images.
                if (deleteExisting)
                {
                    foreach (var file in existingFiles)
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
                        File.Delete(file);
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
                    existingFiles = new string[0];
                }
                else
                {
                    // TODO: Add possibility to add to existing entries
                    //throw new Exception($"To avoid creating duplicate images, the process was aborted as \"{searchTerm}\" images already exist");
                    //index = existingFiles.Select(file => int.Parse(string.Join("", file.Where(char.IsDigit)))).Max();
                    Console.WriteLine($"** Skipping fetching \"{searchTerm}\" images, as some exist already, to avoid creating duplicates");
                    return false;
                }
            }

            var results = imageSearchService.Search(searchTerm, imagesToFetch);

            var success = true;
            using (var webClient = new WebClient())
            {
                foreach (var result in results)
                {
                    Console.Write(".");
                    try
                    {
                        webClient.DownloadFile(result.Url, Path.Combine(finalPath, $"{searchTerm}-{++index:d5}.jpg"));
                    }
                    catch
                    {
                        Console.WriteLine($"ERROR FETCHING: {result.Url}");
                        // NB: Skipping setting false, so that it continue
                        success = false;
                    }
                }
                Console.WriteLine();
            }
            return success;
        }
    }
}
