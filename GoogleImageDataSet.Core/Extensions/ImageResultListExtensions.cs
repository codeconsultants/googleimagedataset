﻿using System.Text;
using GoogleImageDataSet.Core.Interfaces;

namespace GoogleImageDataSet.Core.Extensions
{
    public static class ImageResultListExtensions
    {
        public static string AsImageHtml<TImageResult, TImageResultList>(this TImageResultList imageResultList)
            where TImageResult : IImageResult
            where TImageResultList : IImageResultList<TImageResult>
        {
            var html = new StringBuilder();

            foreach (var imageResult in imageResultList)
            {
                html.AppendLine(imageResult.AsImageHtml<TImageResult>());
            }

            return html.ToString();
        }

        public static string AsImageHtml<TImageResult>(this TImageResult imageResult)
            where TImageResult : IImageResult
            => $"<img src=\"{imageResult.Url}\" />";
    }
}
