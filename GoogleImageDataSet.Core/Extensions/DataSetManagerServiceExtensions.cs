﻿using System;
using System.Linq;
using GoogleImageDataSet.Core.Interfaces;

namespace GoogleImageDataSet.Core.Extensions
{
    public static class DataSetManagerServiceExtensions
    {
        public static bool AddImages<TImageResult>(this IDataSetManagerService<TImageResult> dataSetManagerService,
            string dataSetName, bool train, IImageResultList<TImageResult> imageResultList)
            where TImageResult : IImageResult
            // Add all the images to the image set matching the search term, and return whether they were all successful
            => imageResultList.Aggregate(true, (current, imageResult) => current & dataSetManagerService.AddImage(dataSetName, imageResultList.SearchTerm, train, imageResult));

        public static bool AddImages<TImageResult>(this IDataSetManagerService<TImageResult> dataSetManagerService,
            string dataSetName, double trainingRatio, IImageResultList<TImageResult> imageResultList)
            where TImageResult : IImageResult 
            => imageResultList.IndexedValues()
                .Aggregate(true,
                    (current, indexedResult) => current && dataSetManagerService.AddImage(dataSetName,
                                                imageResultList.SearchTerm,
                                                indexedResult.Index < trainingRatio * imageResultList.Count, indexedResult.Value));

        public static bool SaveDataSet<TImageResult,TDataSetManagerService>(this TDataSetManagerService dataSetManagerService,
            string dataSetName, double trainingRatio, bool overwrite, bool extendExisting,
            params IImageResultList<TImageResult>[] imageResultLists)
            where TImageResult : IImageResult
            where TDataSetManagerService : IDataSetManagerService<TImageResult>
        {
            var exists = dataSetManagerService.Exists<TImageResult, TDataSetManagerService>(dataSetName);
            if (overwrite && exists)
            {
                Console.WriteLine("SaveDataSet called with overwrite, deleting existing dataset");
                exists = !dataSetManagerService.DeleteDataSet(dataSetName);
                if (exists) throw new Exception($"Failed to delete data-set [{dataSetName}]");
            }
            if (!exists)
                dataSetManagerService.CreateDataSet(dataSetName);
            else if(!extendExisting)
                throw new InvalidOperationException(
                    $"A data-set called {dataSetName} already exists, and neither overwrite nor extendExisting were set to true");
            return imageResultLists?
                    .Where(resultList => resultList != null)
                    .Aggregate(true, (current,imageResultList) => current &&
                    dataSetManagerService.AddImages(dataSetName, trainingRatio, imageResultList)) 
                    // If imageResultsLists is null, nothing to add to succeed
                    ?? true;
        }

        public static bool Exists<TImageResult, TDataSetManagerService>(
            this TDataSetManagerService dataSetManagerService, string dataSetName)
            where TImageResult : IImageResult
            where TDataSetManagerService : IDataSetManagerService<TImageResult>
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            return (dataSetManagerService as IHasExists<string>)?.Exists(dataSetName) 
                ?? dataSetManagerService.ListDataSets().Contains(dataSetName);
        }
    }
}
