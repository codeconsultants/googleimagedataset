﻿using Newtonsoft.Json;

namespace GoogleImageDataSet.Core.Models
{
    /// <summary>
    /// A class representing google's image search metadata.
    /// </summary>
    public class GoogleImageMetadata
    {
        [JsonProperty("clt")]
        public string clt { get; set; }
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("isu")]
        public string isu { get; set; }
        [JsonProperty("itg")]
        public int itg { get; set; }
        [JsonProperty("ity")]
        public string ity { get; set; }
        [JsonProperty("oh")]
        public int oh { get; set; }
        [JsonProperty("ou")]
        public string ou { get; set; }
        [JsonProperty("ow")]
        public int ow { get; set; }
        [JsonProperty("pt")]
        public string pt { get; set; }
        [JsonProperty("rh")]
        public string rh { get; set; }
        [JsonProperty("rid")]
        public string rid { get; set; }
        [JsonProperty("rt")]
        public int rt { get; set; }
        [JsonProperty("ru")]
        public string ru { get; set; }
        [JsonProperty("s")]
        public string s { get; set; }
        [JsonProperty("sc")]
        public int sc { get; set; }
        [JsonProperty("st")]
        public string st { get; set; }
        [JsonProperty("th")]
        public int th { get; set; }
        [JsonProperty("tu")]
        public string tu { get; set; }
        [JsonProperty("tw")]
        public int tw { get; set; }
    }
}
