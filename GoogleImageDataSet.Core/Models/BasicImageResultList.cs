﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GoogleImageDataSet.Core.Interfaces;

namespace GoogleImageDataSet.Core.Models
{
    /// <summary>
    /// A basic image result list.
    /// </summary>
    /// <seealso cref="List{BasicImageResult}" />
    /// <seealso cref="IImageResultList{BasicImageResult}" />
    public class BasicImageResultList : List<BasicImageResult>, IImageResultList<BasicImageResult>
    {
        public BasicImageResultList(string searchTerm, IEnumerable<BasicImageResult> imageResults)
            : base(imageResults ?? Enumerable.Empty<BasicImageResult>())
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            SearchTerm = searchTerm;
        }

        protected BasicImageResultList(IEnumerable<BasicImageResult> imageResults = null)
            : this(null, imageResults)
        {
        }

        /// <inheritdoc />
        public virtual string SearchTerm { get; set; }

        public BasicImageResultList AddImages(params string[] imageUrls)
            => AddImages((IEnumerable<string>) imageUrls);
        public BasicImageResultList AddImages(IEnumerable<string> imageUrls)
        {
            AddRange(imageUrls.Select(imageUrl => new BasicImageResult(imageUrl)));
            return this;
        }
    }
}
