﻿using GoogleImageDataSet.Core.Interfaces;

namespace GoogleImageDataSet.Core.Models
{
    /// <summary>
    /// A basic image result.
    /// </summary>
    /// <seealso cref="IImageResult" />
    public class BasicImageResult : IImageResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasicImageResult"/> class.
        /// </summary>
        public BasicImageResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicImageResult"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        public BasicImageResult(string url)
        {
            // ReSharper disable VirtualMemberCallInConstructor
            Url = url;
            // ReSharper restore VirtualMemberCallInConstructor
        }

        /// <summary>
        /// Initializes the image reslt with the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        protected virtual void Initialize(string url)
        {
            Url = url;
        }

        /// <inheritdoc />
        public virtual string Url { get; set; }
    }
}
