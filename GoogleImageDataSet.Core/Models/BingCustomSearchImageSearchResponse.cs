﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GoogleImageDataSet.Core.Models
{
    public class BingCustomSearchImageSearchResponse
    {
        //public string _type { get; set; }
        [JsonProperty("_type")]
        public string Type { get; set; }
        //public Instrumentation instrumentation { get; set; }
        [JsonProperty("instrumentation")]
        public InstrumentationObject Instrumentation { get; set; }
        //public string readLink { get; set; }
        [JsonProperty("readLink")]
        public string ReadLink { get; set; }
        //public string webSearchUrl { get; set; }
        [JsonProperty("webSearchUrl")]
        public string WebSearchUrl { get; set; }
        //public int totalEstimatedMatches { get; set; }
        [JsonProperty("totalEstimatedMatches")]
        public int TotalEstimatedMatches { get; set; }
        //public int nextOffset { get; set; }
        [JsonProperty("nextOffset")]
        public int NextOffset { get; set; }
        //public List<Value> value { get; set; }
        [JsonProperty("value")]
        public List<ValueObject> Value { get; set; }
        //public List<QueryExpansion> queryExpansions { get; set; }
        [JsonProperty("queryExpansions")]
        public List<QueryExpansion> QueryExpansions { get; set; }
        //public List<PivotSuggestion> pivotSuggestions { get; set; }
        [JsonProperty("pivotSuggestions")]
        public List<PivotSuggestion> PivotSuggestions { get; set; }
        //public List<SimilarTerm> similarTerms { get; set; }
        [JsonProperty("similarTerms")]
        public List<SimilarTerm> SimilarTerms { get; set; }
        //public List<RelatedSearch> relatedSearches { get; set; }
        [JsonProperty("relatedSearches")]
        public List<RelatedSearch> RelatedSearches { get; set; }

        public class InstrumentationObject
        {
        }

        public class Thumbnail1
        {
            //public int width { get; set; }
            [JsonProperty("width")]
            public int Width { get; set; }
            //public int height { get; set; }
            [JsonProperty("height")]
            public int Height { get; set; }
        }

        public class InsightsMetadataObject
        {
            //public int recipeSourcesCount { get; set; }
            [JsonProperty("recipeSourcesCount")]
            public int RecipeSourcesCount { get; set; }
        }

        public class ValueObject
        {
            //public string webSearchUrl { get; set; }
            [JsonProperty("webSearchUrl")]
            public string WebSearchUrl { get; set; }
            //public string name { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            //public string thumbnailUrl { get; set; }
            [JsonProperty("thumbnailUrl")]
            public string ThumbnailUrl { get; set; }
            //public DateTime datePublished { get; set; }
            [JsonProperty("datePublished")]
            public DateTime DatePublished { get; set; }
            //public string contentUrl { get; set; }
            [JsonProperty("contentUrl")]
            public string ContentUrl { get; set; }
            //public string hostPageUrl { get; set; }
            [JsonProperty("hostPageUrl")]
            public string HostPageUrl { get; set; }
            //public string contentSize { get; set; }
            [JsonProperty("contentSize")]
            public string ContentSize { get; set; }
            //public string encodingFormat { get; set; }
            [JsonProperty("encodingFormat")]
            public string EncodingFormat { get; set; }
            //public string hostPageDisplayUrl { get; set; }
            [JsonProperty("hostPageDisplayUrl")]
            public string HostPageDisplayUrl { get; set; }
            //public int width { get; set; }
            [JsonProperty("width")]
            public int Width { get; set; }
            //public int height { get; set; }
            [JsonProperty("height")]
            public int Height { get; set; }
            //public Thumbnail thumbnail { get; set; }
            [JsonProperty("thumbnail")]
            public Thumbnail1 Thumbnail { get; set; }
            //public string imageInsightsToken { get; set; }
            [JsonProperty("imageInsightsToken")]
            public string ImageInsightsToken { get; set; }
            //public InsightsMetadata insightsMetadata { get; set; }
            [JsonProperty("insightsMetadata")]
            public InsightsMetadataObject InsightsMetadata { get; set; }
            //public string imageId { get; set; }
            [JsonProperty("imageId")]
            public string ImageId { get; set; }
            //public string accentColor { get; set; }
            [JsonProperty("accentColor")]
            public string AccentColor { get; set; }
        }

        public class Thumbnail2
        {
            //public string thumbnailUrl { get; set; }
            [JsonProperty("thumbnailUrl")]
            public string ThumbnailUrl { get; set; }
        }

        public class QueryExpansion
        {
            //public string text { get; set; }
            [JsonProperty("text")]
            public string Text { get; set; }
            //public string displayText { get; set; }
            [JsonProperty("displayText")]
            public string DisplayText { get; set; }
            //public string webSearchUrl { get; set; }
            [JsonProperty("webSearchUrl")]
            public string WebSearchUrl { get; set; }
            //public string searchLink { get; set; }
            [JsonProperty("searchLink")]
            public string SearchLink { get; set; }
            //public Thumbnail2 thumbnail { get; set; }
            [JsonProperty("thumbnail")]
            public Thumbnail2 Thumbnail { get; set; }
        }

        public class Thumbnail3
        {
            //public string thumbnailUrl { get; set; }
            [JsonProperty("thumbnailUrl")]
            public string ThumbnailUrl { get; set; }
        }

        public class Suggestion
        {
            //public string text { get; set; }
            [JsonProperty("text")]
            public string Text { get; set; }
            //public string displayText { get; set; }
            [JsonProperty("displayText")]
            public string DisplayText { get; set; }
            //public string webSearchUrl { get; set; }
            [JsonProperty("webSearchUrl")]
            public string WebSearchUrl { get; set; }
            //public string searchLink { get; set; }
            [JsonProperty("searchLink")]
            public string SearchLink { get; set; }
            //public Thumbnail3 thumbnail { get; set; }
            [JsonProperty("thumbnail")]
            public Thumbnail3 Thumbnail { get; set; }
        }

        public class PivotSuggestion
        {
            //public string pivot { get; set; }
            [JsonProperty("pivot")]
            public string Pivot { get; set; }
            //public List<Suggestion> suggestions { get; set; }
            [JsonProperty("suggestions")]
            public List<Suggestion> Suggestions { get; set; }
        }

        public class Thumbnail4
        {
            //public string url { get; set; }
            [JsonProperty("url")]
            public string Url { get; set; }
        }

        public class SimilarTerm
        {
            //public string text { get; set; }
            [JsonProperty("text")]
            public string Text { get; set; }
            //public string displayText { get; set; }
            [JsonProperty("displayText")]
            public string DisplayText { get; set; }
            //public string webSearchUrl { get; set; }
            [JsonProperty("webSearchUrl")]
            public string WebSearchUrl { get; set; }
            //public Thumbnail4 thumbnail { get; set; }
            [JsonProperty("thumbnail")]
            public Thumbnail4 Thumbnail { get; set; }
        }

        public class Thumbnail5
        {
            //public string thumbnailUrl { get; set; }
            [JsonProperty("thumbnailUrl")]
            public string ThumbnailUrl { get; set; }
        }

        public class RelatedSearch
        {
            //public string text { get; set; }
            [JsonProperty("text")]
            public string Text { get; set; }
            //public string displayText { get; set; }
            [JsonProperty("displayText")]
            public string DisplayText { get; set; }
            //public string webSearchUrl { get; set; }
            [JsonProperty("webSearchUrl")]
            public string WebSearchUrl { get; set; }
            //public string searchLink { get; set; }
            [JsonProperty("searchLink")]
            public string SearchLink { get; set; }
            //public Thumbnail5 thumbnail { get; set; }
            [JsonProperty("thumbnail")]
            public Thumbnail5 Thumbnail { get; set; }
        }
    }
}
