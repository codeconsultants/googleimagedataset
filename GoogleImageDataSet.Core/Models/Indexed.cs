﻿using GoogleImageDataSet.Core.Interfaces;
namespace GoogleImageDataSet.Core.Models
{
    /// <summary>
    /// Gets a typed, indexed value.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    /// <seealso cref="IIndexed" />
    public class Indexed<TValue> : IIndexed<TValue>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Indexed{TValue}"/> class.
        /// </summary>
        public Indexed()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Indexed{TValue}"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="value">The value.</param>
        public Indexed(int index, TValue value)
        {
            Index = index;
            Value = value;
        }

        /// <inheritdoc />
        public int Index { get; set; }
        /// <inheritdoc />
        public TValue Value { get; set; }
    }
}
