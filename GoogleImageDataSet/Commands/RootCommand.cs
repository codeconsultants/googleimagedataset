﻿using GoogleImageDataSet.Extensions;
using Microsoft.Extensions.CommandLineUtils;

namespace GoogleImageDataSet.Commands
{
    public class RootCommand : ICommand
    {
        protected readonly CommandLineApplication App;

        public RootCommand(CommandLineApplication app)
        {
            App = app;
        }

        public const string StandardHelpOption = "-?|-h|--help";
        public const string ExtendedHelpText = @"
This is a tool designed by James Burton @ Code-Consultants to facilitate creating quick test image datasets using Google Image Search.
";
        public string Name => "GoogleImageDataSet";
        public void Run() => App.ShowHelp();

        public static CommandLineApplication Configure() => Configure(new CommandLineApplication());
        public static CommandLineApplication Configure(CommandLineApplication app)
        {
            app.Name = "GoogleImageDataSet";
            app.HelpOption(StandardHelpOption);
            app.ExtendedHelpText = ExtendedHelpText;

            // Register commands
            app
                .ConfigureCommand<ListCommand>()
                .ConfigureCommand<CreateCommand>()
                .ConfigureCommand<AddCommand>()
                .ConfigureCommand<SearchCommand>()
                ;

            return app;
        }
    }
}
