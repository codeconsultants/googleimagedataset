﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;
using GoogleImageDataSet.Core.Extensions;
using GoogleImageDataSet.Core.Models;
using static GoogleImageDataSet.Core.Constants;

namespace GoogleImageDataSet.Commands
{
    public class AddCommand : ICommand
    {
        #region Required static properties and methods
        public static string Name => "add";
        public static void Configure(CommandLineApplication app)
        {
            app.Description = "Adds a search to a data-set";
            app.HelpOption(RootCommand.StandardHelpOption);

            var dataSetNameArgument = app.Argument("dataSetName", "The name for the new data-set");
            var searchTermsArgument = app.Argument("searchTerm [searchTerm ...]", "The search term(s) to add as a category or categories in the data-set", true);
            var imagesToFetchOption = app.Option("-i|--images-per-searcb <numberPerSearch>", $"The number of images to fetch, default={DefaultImagesToFetch}", CommandOptionType.SingleValue);
            var trainingRatioOption = app.Option("-t|--training-ratio <trainingRatio>",
                $"The ratio of images to store in train/ vs valid/, default={DefaultTrainingRatio}", CommandOptionType.SingleValue);

            app.OnExecute(() =>
            {
                if (string.IsNullOrEmpty(dataSetNameArgument.Value) || string.IsNullOrEmpty(searchTermsArgument.Value))
                    app.ShowHelp(Name);
                else
                    new AddCommand(dataSetNameArgument.Value, searchTermsArgument.Values, double.Parse(trainingRatioOption.Value() ?? DefaultTrainingRatio.ToString()), int.Parse(imagesToFetchOption.Value() ?? DefaultImagesToFetch.ToString())).Run();
                return 0;
            });
        }
        #endregion Required static properties and methods

        public string DataSetName { get; protected set; }
        public IList<string> SearchTerms { get; protected set; }
        public double TrainingRatio { get; protected set; }
        public int ImagesToFetch { get; protected set; }
        public AddCommand(string dataSetName, IList<string> searchTerms, double trainingRatio, int imagesToFetch)
        {
            DataSetName = dataSetName;
            SearchTerms = searchTerms;
            ImagesToFetch = imagesToFetch;
            TrainingRatio = trainingRatio;
        }

        #region Implementation of ICommand (e.g. .Run())
        public void Run()
        {
            Console.WriteLine($"Amending data-set [{DataSetName}]");
            Console.WriteLine($"Adding search-term categories: {string.Join(", ", SearchTerms)}");
            var dataSetService = Program.DataSetManagerService;
            var imageSearchService = Program.ImageSearchService;
            var success = SearchTerms.Aggregate(true,
                (current, searchTerm) => current &&
                    imageSearchService.AddToDataSet<BasicImageResult,BasicImageResultList>(dataSetService, DataSetName, searchTerm, TrainingRatio, imagesToFetch: ImagesToFetch));
            Console.WriteLine(success ? "- OK" : " - FAIL");
        }
        #endregion Implementation of ICommand (e.g. .Run())
    }
}
