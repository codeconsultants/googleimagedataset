﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.CommandLineUtils;

namespace GoogleImageDataSet.Commands
{
    public class CreateCommand : ICommand
    {
        #region Required static properties and methods
        public static string Name => "create";
        public static void Configure(CommandLineApplication app)
        {
            app.Description = "Creates a dataset";
            app.HelpOption(RootCommand.StandardHelpOption);

            var dataSetNameArgument = app.Argument("[dataSetName]", "The name for the new data-set");

            app.OnExecute(() =>
            {
                if (string.IsNullOrEmpty(dataSetNameArgument.Value))
                    app.ShowHelp(Name);
                else
                    new CreateCommand(dataSetNameArgument.Value).Run();
                return 0;
            });
        }
        #endregion Required static properties and methods

        public string DataSetName { get; protected set; }
        public CreateCommand(string dataSetName)
        {
            DataSetName = dataSetName;
        }

        #region Implementation of ICommand (e.g. .Run())
        public void Run()
        {
            Console.WriteLine($"Creating data-set [{DataSetName}]");
            var dataSetService = Program.DataSetManagerService;
            var success = dataSetService.CreateDataSet(DataSetName);
            Console.WriteLine(success ? "- OK" : " - FAIL");
        }
        #endregion Implementation of ICommand (e.g. .Run())
    }
}
