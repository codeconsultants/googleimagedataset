﻿namespace GoogleImageDataSet.Commands
{
    /// <summary>
    /// An interface for CLI commands
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// Runs this command.
        /// </summary>
        void Run();
    }
}
