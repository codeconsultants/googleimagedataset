﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;
using GoogleImageDataSet.Core.Extensions;
using GoogleImageDataSet.Core.Models;
using GoogleImageDataSet.Extensions;
using static GoogleImageDataSet.Core.Constants;

namespace GoogleImageDataSet.Commands
{
    public class SearchCommand : ICommand
    {
        #region Required static properties and methods
        public static string Name => "search";
        public static void Configure(CommandLineApplication app)
        {
            app.Description = "Saves images for the specified search terms";
            app.HelpOption(RootCommand.StandardHelpOption);

            var imagesToFetchOption = app.Option("-i|--images-per-searcb <numberPerSearch>", $"The number of images to fetch, default={DefaultImagesToFetch}", CommandOptionType.SingleValue);
            ////throw new Exception(string.Join(", ", StringExtensions.TrueAndFalseValues));
            //var createSubFoldersOptions = app.Option("-s|--subfolders <boolValue>", "Whether to create subfolders for the search terms, default true", CommandOptionType.SingleValue, config => config.Values.AddRange(StringExtensions.TrueAndFalseValues));
            var createSubFoldersOptions = app.Option("-s|--subfolders <boolValue>", "Whether to create subfolders for the search terms, default true", CommandOptionType.SingleValue);
            var deleteExistingImages = app.Option("-d|--delete-existing",
                "Sets to delete existing images of the output naming pattern before saving", CommandOptionType.NoValue);
            var outputPathArgument = app.Argument("outputPath", "The folder in which to save the images");
            var searchTermsArgument = app.Argument("searchTerm [searchTerm ...]", "The search term(s) to add as a category or categories in the data-set", true);

            app.OnExecute(() =>
            {
                if (string.IsNullOrEmpty(outputPathArgument.Value) || string.IsNullOrEmpty(searchTermsArgument.Value))
                    app.ShowHelp(Name);
                else
                    new SearchCommand(outputPathArgument.Value, int.Parse(imagesToFetchOption.Value() ?? DefaultImagesToFetch.ToString()), createSubFoldersOptions.Value().BoolParamValue(defaultIfNull: true), deleteExistingImages.HasValue(), searchTermsArgument.Values).Run();
                return 0;
            });
        }
        #endregion Required static properties and methods

        public string OutputPath { get; protected set; }
        public int ImagesToFetch { get; protected set; }
        public bool CreateSubFolders { get; protected set; }
        public bool DeleteExisting { get; protected set; }
        public IList<string> SearchTerms { get; protected set; }
        public SearchCommand(string outputPath, int imagesToFetch, bool createSubFolders, bool deleteExisting, IList<string> searchTerms)
        {
            OutputPath = outputPath;
            ImagesToFetch = imagesToFetch;
            CreateSubFolders = createSubFolders;
            DeleteExisting = deleteExisting;
            SearchTerms = searchTerms;
        }

        #region Implementation of ICommand (e.g. .Run())
        public void Run()
        {
            Console.WriteLine($"Saving images to [{OutputPath}]");
            Console.WriteLine($"- Using the following search-terms: {string.Join(", ", SearchTerms)}");
            var dataSetService = Program.DataSetManagerService;
            var imageSearchService = Program.ImageSearchService;
            var success = SearchTerms.Aggregate(true,
                (current, searchTerm) => current &&
                    imageSearchService.SaveToFolder<BasicImageResult,BasicImageResultList>(OutputPath, ImagesToFetch, CreateSubFolders, DeleteExisting, searchTerm));
            Console.WriteLine(success ? "- OK" : " - FAIL");
        }
        #endregion Implementation of ICommand (e.g. .Run())
    }
}
