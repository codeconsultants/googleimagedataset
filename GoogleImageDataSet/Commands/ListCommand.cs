﻿using System;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;

namespace GoogleImageDataSet.Commands
{
    public class ListCommand : ICommand
    {
        #region Required static properties and methods
        public static string Name => "list";
        public static void Configure(CommandLineApplication app)
        {
            app.Description = "List the available datasets, or categories within a dataset";
            app.HelpOption(RootCommand.StandardHelpOption);

            var dataSetNameArgument = app.Argument("[dataSetName]",
                "Specify a dataset name to list its existing categories");

            app.OnExecute(() =>
            {
                new ListCommand(dataSetNameArgument.Value).Run();
                return 0;
            });
        }
        #endregion Required static properties and methods

        public string DataSetName { get; protected set; }
        public ListCommand(string dataSetName)
        {
            DataSetName = dataSetName;
        }

        #region Implementation of ICommand (e.g. .Run())
        public void Run()
        {
            if (DataSetName == null)
            {
                // List Data-Sets, as no set was named to list within
                Console.WriteLine("Listing available datasets:");
                //var imageSearchService = Program.ImageSearchService;
                var dataSetService = Program.DataSetManagerService;
                foreach (var dataSet in dataSetService.ListDataSets())
                {
                    Console.WriteLine($"- {dataSet}");
                }
            }
            else
            {
                Console.WriteLine($"Listing contents of dataset [{DataSetName}]");
                var dataSetManagerService = Program.DataSetManagerService;
                var categories = dataSetManagerService.ListCategories(DataSetName);
                if (categories.Any())
                    foreach (var category in categories)
                        Console.WriteLine($"- {category}");
                else
                    Console.WriteLine("- [No categories found]");
            }
        }
        #endregion Implementation of ICommand (e.g. .Run())
    }
}
