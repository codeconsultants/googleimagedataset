﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using GoogleImageDataSet.Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace GoogleImageDataSet
{
    using GoogleImageDataSet.Core.Extensions;
    using GoogleImageDataSet.Core.Interfaces;
    using GoogleImageDataSet.Core.Models;
    using GoogleImageDataSet.Core.Services;
    internal class Program
    {
        private static readonly Lazy<IImageSearchService<BasicImageResult,BasicImageResultList>> LazyImageSearchService = new Lazy<IImageSearchService<BasicImageResult,BasicImageResultList>>(() => new GoogleImageSearchService());
        public static IImageSearchService<BasicImageResult,BasicImageResultList> ImageSearchService => LazyImageSearchService.Value;

        private static readonly Lazy<IDataSetManagerService<BasicImageResult>> LazyDataSetManagerService = new Lazy<IDataSetManagerService<BasicImageResult>>(() => new ZipDataSetManagerService(".\\datasets\\"));
        public static IDataSetManagerService<BasicImageResult> DataSetManagerService => LazyDataSetManagerService.Value;

        static void Main(string[] args)
        {
            Console.WriteLine("GoogleImageDataSet CLI tool, by James Burton @ Code-Consultants");

            var imageSearchService = ImageSearchService;
            var dataSetService = DataSetManagerService;

            var app = RootCommand.Configure();
            app.Command("help", appParam => app.ShowHelp());
            app.Execute(args);
        }
    }
}
