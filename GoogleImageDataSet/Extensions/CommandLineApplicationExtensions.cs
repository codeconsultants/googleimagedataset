﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using GoogleImageDataSet.Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace GoogleImageDataSet.Extensions
{
    public static class CommandLineApplicationExtensions
    {
        public static CommandLineApplication ConfigureCommand<TCommand>(this CommandLineApplication app)
            where TCommand : ICommand
        {
            //var configure = (Action<CommandLineApplication>) 
            //    Delegate.CreateDelegate(typeof(TCommand), typeof(TCommand).GetMethod("Configure", BindingFlags.Public | BindingFlags.Static));
            //app.Command(CommandExtensions.GetName<TCommand>(), configure);

            var configureMethod = typeof(TCommand).GetMethod("Configure", BindingFlags.Public | BindingFlags.Static);
            app.Command(CommandExtensions.GetName<TCommand>(), appParam => configureMethod.Invoke(null, new object[] { appParam }));

            return app;
        }
    }
}
