﻿using System.Reflection;

namespace GoogleImageDataSet.Extensions
{
    public static class CommandExtensions
    {
        public static string GetName<TCommand>()
            => typeof(TCommand).GetProperty("Name", BindingFlags.Public | BindingFlags.Static).GetValue(null) as string;
    }
}
