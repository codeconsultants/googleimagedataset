﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoogleImageDataSet.Extensions
{
    /// <summary>
    /// A static class providing extension methods to strings. 
    /// </summary>
#pragma warning disable ET001 // Type name does not match file name
    public static partial class StringExtensions
#pragma warning restore ET001 // Type name does not match file name
    {
        public static readonly List<string> TrueValues = new List<string> { "1", "True", "true", "Yes", "yes", "Y", "y" };
        public static readonly List<string> FalseValues = new List<string> { "0", "False", "false", "No", "no", "N", "n" };
        public static Lazy<List<string>> LazyTrueAndFalseValues = new Lazy<List<string>>(() => new List<string>(TrueValues.Concat(FalseValues)));
        public static List<string> TrueAndFalseValues = LazyTrueAndFalseValues.Value;

        public static bool? NullableBoolParamValue(this string sourceString, bool errorIfInvalid = true, bool? defaultIfInvalid = null)
        {
            if (string.IsNullOrEmpty(sourceString)) return null;
            if (TrueValues.Contains(sourceString)) return true;
            if (FalseValues.Contains(sourceString)) return false;
            if (errorIfInvalid) throw new Exception($"sourceString does not contain a valid/known boolean value: \"{sourceString}\"");
            return defaultIfInvalid;
        }

        public static bool BoolParamValue(this string sourceString, bool errorIfInvalid = true, bool? defaultIfInvalid = false,
            bool errorIfNull = false, bool defaultIfNull = false)
        {
            var b = sourceString.NullableBoolParamValue(errorIfInvalid, defaultIfInvalid);
            if(b == null && errorIfNull) throw new NullReferenceException($"sourceString was null or invalid: \"{sourceString}\"");
            return b ?? defaultIfNull;
        }
    }
}
