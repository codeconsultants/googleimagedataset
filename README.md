# GoogleImageDataSet
## Written by James Burton @ Code-Consultants & Seedr.Space

This tool is designed to facilitate grabbing thumbnails from image search, and gathering into training and validation set for AI/ML usage.  This is ideal for learning to build image classifiers, such as the techniques taught over at https://course.fast.ai

## License

The licensing is still a work-in-progress, but I do not intend to charge non-commercial and non-profit users.  Commercial usage requires a license, so please contact me at james@code-consultants.co.uk to discuss this if you require licensing before I have updated these details, but 30 days test usage is allowed before licensing to ensure this product suits requirements.

## Projects

* GoogleImageDataSet.Core is the main library, which handles image searching and data set aggregation/management, and it is implemented using netStandard 2.0.
* GoogleImageDataSet is the CLI wrapper around the core functionality.
* GoogleImageDataSet.MVC is the (coming soon) server wrapper, provisioning this service via an API and a simple web-interface.

## Requirements

* .NET Core Framework SDK v2.1
* (Optional) Visual Studio 2017

## Building

#### Building at command line (all platforms)

* Clone the repository\
```git clone https://bitbucket.org/codeconsultants/googleimagedataset/```
* Navigate to the solution folder\
```cd GoogleImageDataSet```
* Build with dotnet CLI (at Command Prompt)
```
dotnet build GoogleImageDataSet.Core\GoogleImageDataSet.Core.csproj
dotnet build GoogleImageDataSet\GoogleImageDataSet.csproj
```
* Run CLI command (to view SYNTAX/USAGE)\
```dotnet GoogleImageDataSet\bin\debug\netcoreapp2.1\GoogleImageDataSet.dll```

#### Building using Visual Studio 2017 (windows only)

* Clone the repository
* Open the solution file in the root folder (GoogleImageDataSet.sln)
* Press Ctrl+Shift+B, or select Build->Build Solution from the menu

## Usage

### CLI Usage
* Call CLI command from DLL using ```dotnet <dllPath> ...```
* Call CLI without parameters to view commands, and use ```help``` command or ```--help``` option to view info.
* Call ```dotnet GoogleImageDataSet.dll list``` to list current datasets
* Call ```dotnet GoogleImageDataSet.dll list <dataSetName>``` to list categories in a data-set
* Call ```dotnet GoogleImageDataSet.dll add -i 1000 <dataSetName> <searchTerm1> <searchTerm2> [<searchTerm3> ... <searchTermN>]``` to attempt to fetch 1000 images per search term, add them to a dataset (in ./datasets/<dataSetName>.zip)

### GoogleImageDataSet.Core Usage
* Please review the CLI project for examples of calling the Core library

## Building standalone CLI executables

NB: Omit "-c Release" if you wish to output Debug versions

* Build Win 32-bit
```
dotnet build GoogleImageDataSet\GoogleImageDataSet.csproj -c Release -r Win-x86
```
* Build Win 64-bit
```
dotnet build GoogleImageDataSet\GoogleImageDataSet.csproj -c Release -r Win-x64
```
* Build Linux (portable)
```
dotnet build GoogleImageDataSet\GoogleImageDataSet.csproj -c Release -r linux-x64
```
## References

* Build platforms for dotnet: https://docs.microsoft.com/en-us/dotnet/core/rid-catalog 